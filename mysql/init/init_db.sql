CREATE DATABASE IF NOT EXISTS 'laravel'
CREATE DATABASE IF NOT EXISTS 'laravel_test'

# create root user and grant rights
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%';